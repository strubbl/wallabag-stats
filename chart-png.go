package main

import (
	"bytes"
	"fmt"
	"log"
	"os"

	"github.com/wcharczuk/go-chart/v2"
)

func generateChartPNG(wbgStats *WallabagStats, pictureName string) error {
	if *dd {
		log.Printf("generateChartPNG: start generating chart in file pictureName=%v", pictureName)
	}
	if *ddd {
		log.Printf("generateChartPNG: wbgStats: \n%v", wbgStats)
	}

	graph := chart.Chart{
		XAxis: chart.XAxis{
			Name:           "Time",
			ValueFormatter: chart.TimeHourValueFormatter,
		},
		YAxis: chart.YAxis{
			Name: "Unread",
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%0.0f", vf)
				}
				return ""
			},
		},
		YAxisSecondary: chart.YAxis{
			Name:  "Total",
			Style: chart.Style{},
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%0.0f", vf)
				}
				return ""
			},
		},
		Background: chart.Style{
			Padding: chart.Box{
				Top:  20,
				Left: 20,
			},
		},
		Series: []chart.Series{
			chart.TimeSeries{
				Name:    "Unread",
				XValues: wbgStats.Times,
				YValues: wbgStats.Unread,
			},
			chart.TimeSeries{
				YAxis:   chart.YAxisSecondary,
				Name:    "Total",
				XValues: wbgStats.Times,
				YValues: wbgStats.Total,
			},
		},
	}
	if *dd {
		log.Println("generateChartPNG: chart created")
	}
	graph.Elements = []chart.Renderable{
		chart.Legend(&graph),
	}
	if *dd {
		log.Println("generateChartPNG: add legend to chart")
	}

	buffer := bytes.NewBuffer([]byte{})
	err := graph.Render(chart.PNG, buffer)
	if err != nil {
		if *dd {
			log.Println("generateChartPNG: error while rendering graph")
		}
		fmt.Println(err)
		return err
	}
	err = os.WriteFile(pictureName, buffer.Bytes(), 0644)
	if err != nil {
		if *dd {
			log.Println("generateChartPNG: error while writing png")
		}
		fmt.Println(err)
		return err
	}
	return nil
}
