package main

import (
	"log"
	"os"
	"time"

	"github.com/Strubbl/wallabago/v9"
)

func main() {
	start := time.Now()
	defer printElapsedTime(start)

	log.SetOutput(os.Stdout)
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	handleFlags()

	if *d {
		log.Printf("wallabag-stats version %s, commit %s, built at %s by %s", version, commit, date, builtBy)
	}

	// check if lock file exists and exit, so we do not run this process two times
	if _, err := os.Stat(lockFile); os.IsNotExist(err) {
		if *dd {
			log.Printf("main: no lockfile %s present", lockFile)
		}
	} else {
		log.Printf("abort: lock file exists %s\n", lockFile)
		os.Exit(1)
	}

	// create lock file and delete it on exit of main
	err := os.WriteFile(lockFile, nil, 0644)
	if err != nil {
		if *dd {
			log.Println("main: error while writing lock file")
		}
		panic(err)
	}
	defer removeLockFile(lockFile)

	if *d {
		log.Println("reading config", *configJSON)
	}
	err = wallabago.ReadConfig(*configJSON)
	if err != nil {
		log.Println(err.Error())
		removeLockFile(lockFile)
		os.Exit(1)
	}

	if *d {
		log.Println("reading data json file into memory")
	}
	var wbgStats WallabagStats
	err = readCurrentJSON(&wbgStats)
	if err != nil {
		removeLockFile(lockFile)
		panic(err)
	}

	if *validateData {
		if *d {
			log.Println("validating data")
		}
		err := saveValidatedData(&wbgStats)
		removeLockFile(lockFile)
		if err != nil {
			panic(err)
		}
		os.Exit(0)
	}

	if *d {
		log.Println("get current stats data set from Wallabag")
	}
	tmpTotal, err := wallabago.GetNumberOfTotalArticles()
	if err != nil {
		log.Printf("got error while getting total number of articles: %v", err)
	}
	total := float64(tmpTotal)
	tmpArchived, err := wallabago.GetNumberOfArchivedArticles()
	if err != nil {
		log.Printf("got error while getting total number of archived articles: %v", err)
	}
	archived := float64(tmpArchived)
	tmpStarred, err := wallabago.GetNumberOfStarredArticles()
	if err != nil {
		log.Printf("got error while getting total number of starred articles: %v", err)
	}
	starred := float64(tmpStarred)
	unread := float64(total - archived)
	if *dd {
		log.Println("main: total:", total, ", archived:", archived, ", unread:", unread, ", starred:", starred, ", time:", time.Now())
	}
	if *ddd {
		log.Printf("main: wbgStats: %v\n", wbgStats)
	}

	wasChartGenerated := generateOutputIfNewData(&wbgStats, total, archived, unread, starred)

	err = generateArticleReadingTimeStats()
	if err != nil {
		log.Println("error during generating article reading time statistics:", err)
		removeLockFile(lockFile)
		os.Exit(1)
	}

	if *forceChart && !wasChartGenerated {
		if *d {
			log.Print("generating output because of force flag")
		}
		generateOutput(&wbgStats)
	}
	if *d {
		log.Print("main program finish")
	}
}
