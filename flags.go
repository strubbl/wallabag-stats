package main

import (
	"flag"
	"log"
	"os"
	"strings"
)

var dataOnly = flag.Bool("data-only", false, "collect data only, do not generate any charts")
var forceChart = flag.Bool("f", false, "force regeneration of charts and html even if no new data is present")
var validateData = flag.Bool("validate", false, "get through every data item and sort invalid data entries out")
var d = flag.Bool("d", false, "debug mode")
var dd = flag.Bool("dd", false, "more debug mode")
var ddd = flag.Bool("ddd", false, "even more debug mode")
var v = flag.Bool("v", false, "print version")
var configJSON = flag.String("config", defaultConfigJSON, "file name of config JSON file")
var dataJSON = flag.String("data", defaultDataJSON, "file name of data JSON file")
var articleStatsStartDate = flag.String("article-stats-start-date", defaultArticleStatsStartDate, "date when the article stats shall start, it has to be provided as string with the RFC3339 format 2006-01-02T15:04:05Z07:00")

func handleFlags() {
	flag.Parse()
	if *dd && len(flag.Args()) > 0 {
		log.Printf("handleFlags: non-flag args=%v", strings.Join(flag.Args(), " "))
	}
	// version first, because it directly exits here
	if *v {
		log.Printf("wallabag-stats version %s, commit %s, built at %s by %s", version, commit, date, builtBy)
		os.Exit(0)
	}
	// test verbose before debug because debug implies verbose
	if *d && !*dd && !*ddd {
		log.Printf("verbose mode")
	}
	if *dd && !*ddd {
		log.Printf("handleFlags: debug mode")
		// debug implies verbose
		*d = true
	}
	if *ddd {
		log.Printf("handleFlags: debug² mode")
		// debugDebug implies debug
		*dd = true
		// and debug implies verbose
		*d = true
	}
}
