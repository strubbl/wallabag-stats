package main

import (
	"log"
	"sort"
	"strconv"
	"time"

	"github.com/Strubbl/wallabago/v9"
)

func createArticleStats(id int, readingTime int, archivedAt *wallabago.WallabagTime, foundArticleWithDateBeforeStartDate *bool) (ArticleStats, error) {
	var a ArticleStats
	if readingTime == 0 {
		readingTime = 1
	}
	astatsStartDate, err := getArticleStatsStartDate()
	if err != nil {
		if *dd {
			log.Println("createArticleStats: error while getArticleStatsStartDate:", err)
		}
		return a, err
	}
	if archivedAt.Before(astatsStartDate) {
		// if *debug {
		// 	log.Printf("archivedAt.Before(astatsStartDate), so %v is before %v", archivedAt, astatsStartDate)
		// }
		*foundArticleWithDateBeforeStartDate = true
		return a, nil
	}
	a = ArticleStats{id, readingTime, archivedAt.Time}
	if *ddd {
		log.Println(a)
	}
	return a, nil
}

func getArticleStatsStartDate() (time.Time, error) {
	statsDate, err := time.Parse(time.RFC3339, *articleStatsStartDate)
	return statsDate, err
}

func appendArticleStatsOrNot(articleStatsList *[]ArticleStats, articleStatsArticleIDs *map[int]bool, id int, readingTime int, archivedAt *wallabago.WallabagTime, foundArticleKnownOrNotUsableForStats *bool) {
	asa := *articleStatsArticleIDs
	isArticleInStats := asa[id]
	if id > 0 && !isArticleInStats {
		a, err := createArticleStats(id, readingTime, archivedAt, foundArticleKnownOrNotUsableForStats)
		if err != nil {
			if *dd {
				log.Println("appendArticleStatsOrNot: createArticleStats failed", err)
			}
			return
		}
		*articleStatsList = append(*articleStatsList, a)
		// also update article ID list of article stats
		asa[id] = true
	}
}

func generateArticleReadingTimeStats() error {
	if *dd {
		log.Println("generateArticleReadingTimeStats")
	}
	foundArticleWithDateBeforeStartDate := false // skip articles which have a archived at date before ArticleStatsStartDate
	var astats []ArticleStats
	err := readCurrentJSON(&astats)
	if err != nil {
		if *dd {
			log.Println("generateArticleReadingTimeStats: reading json failed", err)
		}
		return err
	}
	if *dd {
		log.Println("generateArticleReadingTimeStats: length of astats after reading from json: " + strconv.Itoa((len(astats))))
	}
	// cache all article IDs known from []ArticleStats into a map for faster lookup
	astatsIDs := make(map[int]bool)
	for i := 0; i < len(astats); i++ {
		astatsIDs[astats[i].ArticleID] = true
	}

	// variables for the API call to get all archived articles sorted by the date when they were archived beginning with the last read article
	archived := 1
	starred := -1
	sortBy := "archived"
	orderBy := "desc"
	page := -1
	// increase perPage limit of articles to have less API calls
	perPage := perPageLimit
	tags := ""
	since := -1
	public := -1
	detail := ""
	domainName := ""
	// get all archived articles
	e, err := wallabago.GetEntries(wallabago.APICall, archived, starred, sortBy, orderBy, page, perPage, tags, since, public, detail, domainName)
	if err != nil {
		if *dd {
			log.Println("generateArticleReadingTimeStats: first GetEntries call failed", err)
		}
		return err
	}
	for i := 0; i < len(e.Embedded.Items); i++ {
		appendArticleStatsOrNot(&astats, &astatsIDs, e.Embedded.Items[i].ID, e.Embedded.Items[i].ReadingTime, e.Embedded.Items[i].ArchivedAt, &foundArticleWithDateBeforeStartDate)
	}
	if e.Total > len(astats) {
		secondPage := e.Page + 1
		perPage = e.Limit
		pages := e.Pages
		for page = secondPage; page <= pages; page++ {
			if foundArticleWithDateBeforeStartDate {
				if *d {
					log.Println("found the first article before the start date", *articleStatsStartDate)
				}
				break
			}
			if *dd {
				log.Println("fetching the next", perPage, "articles from page", page, "out of total", pages, "pages now")
			}
			e, err := wallabago.GetEntries(wallabago.APICall, archived, starred, sortBy, orderBy, page, perPage, tags, since, public, detail, domainName)
			if err != nil {
				log.Printf("generateArticleReadingTimeStats: GetEntries for page %d failed: %v", page, err)
				return err
			}
			for i := 0; i < len(e.Embedded.Items); i++ {
				appendArticleStatsOrNot(&astats, &astatsIDs, e.Embedded.Items[i].ID, e.Embedded.Items[i].ReadingTime, e.Embedded.Items[i].ArchivedAt, &foundArticleWithDateBeforeStartDate)
			}
		}
	}
	if *dd {
		log.Println("generateArticleReadingTimeStats: length of astats: " + strconv.Itoa((len(astats))))
	}
	sort.Slice(astats, func(i, j int) bool {
		return astats[i].ReadTime.After(astats[j].ReadTime)
	})
	err = writeNewJSON(astats)
	if err != nil {
		log.Printf("generateArticleReadingTimeStats: error writing json: %v", err)
		return err
	}
	err = generateChartData(astats)
	if err != nil {
		log.Printf("generateArticleReadingTimeStats: error while generating chart data: %v", err)
		return err
	}
	return nil
}

func getIndexOfPeriodIDFromLastXDaysData(periodID string, lastXDays []LastXData) int {
	for i := 0; i < len(lastXDays); i++ {
		if lastXDays[i].Period == periodID {
			if *ddd {
				log.Println("getIndexOfPeriodIDFromLastXDaysData: found dayID", periodID)
			}
			return i
		}
	}
	return -1
}

func generateLastXPeriodData(xPeriod int, astats *[]ArticleStats, chartType int, dateFormat string) []LastXData {
	today := time.Now()
	workDate := today
	if *dd {
		log.Println("generateLastXPeriodData: xPeriod:", xPeriod, ", chartType:", chartType, ", dateFormat:", dateFormat, ", workDate:", workDate)
	}
	addDateDays := 0
	addDateMonths := 0
	addDateYears := 0
	switch chartType {
	case chartDays:
		addDateDays = -1
	case chartMonths:
		addDateMonths = -1
		// using first of Month to work around the AddDate normalization, especially usefull when the current month has more days than the month before
		workDate = today.AddDate(0, 0, -today.Day()+1)
	case chartYears:
		addDateYears = -1
		// using first of Month to work around the AddDate normalization, especially usefull when the current year has more days than the year before
		workDate = today.AddDate(0, 0, -today.Day()+1)
	default:
		log.Fatalln("reached default case of chart type switch statement")
	}
	var lastXPeriodData []LastXData
	// create empty set of last x period
	for i := xPeriod; i >= 0; i-- {
		periodX := workDate.AddDate(i*addDateYears, i*addDateMonths, i*addDateDays)
		periodID := periodX.Format(dateFormat)
		if *ddd {
			log.Println("generateLastXPeriodData: periodX:", periodID)
		}
		lastXPeriodData = append(lastXPeriodData, LastXData{periodID, 0, 0})
	}
	for i := 0; i < len(*astats); i++ {
		key := (*astats)[i].ReadTime.Format(dateFormat)
		periodIndex := getIndexOfPeriodIDFromLastXDaysData(key, lastXPeriodData)
		if periodIndex >= 0 {
			lastXPeriodData[periodIndex].Minutes = lastXPeriodData[periodIndex].Minutes + (*astats)[i].ReadingTimeInMinutes
			lastXPeriodData[periodIndex].Number = lastXPeriodData[periodIndex].Number + 1
		} else {
			if *dd {
				log.Println("generateLastXPeriodData: periodIndex does not exist, so we are done with summing up reading times of the last x days:", key)
			}
			break
		}
	}
	return lastXPeriodData
}

func generateChartData(astats []ArticleStats) error {
	dayFormat := "2006-01-02"
	lastXDays := generateLastXPeriodData(xDays, &astats, chartDays, dayFormat)
	if *dd {
		log.Println("generateChartData: lastXDays stats", lastXDays)
	}
	monthFormat := "2006-01"
	lastXMonths := generateLastXPeriodData(xMonths, &astats, chartMonths, monthFormat)
	if *dd {
		log.Println("generateChartData: lastXMonths stats", lastXMonths)
	}
	yearFormat := "2006"
	lastXYears := generateLastXPeriodData(xYears, &astats, chartYears, yearFormat)
	if *dd {
		log.Println("generateChartData: lastXYears stats", lastXYears)
	}

	var articleStatsChartData ArticleStatsChartData
	articleStatsChartData.LastXDays = lastXDays
	articleStatsChartData.LastXMonths = lastXMonths
	articleStatsChartData.LastXYears = lastXYears
	err := writeNewJSON(articleStatsChartData)
	if err != nil {
		log.Println("generateChartData: error writing articleStatsChartData")
		return err
	}
	return nil
}
