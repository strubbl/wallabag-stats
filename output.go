package main

import (
	"log"
	"os"
	"time"
)

func getLastModTime(filePath string) time.Time {
	fi, err := os.Stat(filePath)
	if err != nil {
		log.Println("getLastModTime", err)
		return time.Time{}
	}
	return fi.ModTime()
}

func getWallabagStatsSubset(wbgStats *WallabagStats, duration time.Duration) WallabagStats {
	var subset WallabagStats
	since := time.Now().Add(duration)
	if *dd {
		log.Printf("getWallabagStatsSubset: since=%v", since)
	}
	var sinceDataSetNumber int
	for i := 1; i < len(wbgStats.Times); i++ {
		if wbgStats.Times[i].After(since) && wbgStats.Times[i-1].Before(since) {
			sinceDataSetNumber = i
			break
		}
	}
	if *dd {
		log.Printf("getWallabagStatsSubset: sinceDataSetNumber=%v", sinceDataSetNumber)
	}
	for i := sinceDataSetNumber; i < len(wbgStats.Times); i++ {
		subset.Times = append(subset.Times, wbgStats.Times[i])
		subset.Total = append(subset.Total, wbgStats.Total[i])
		subset.Unread = append(subset.Unread, wbgStats.Unread[i])
		subset.Starred = append(subset.Starred, wbgStats.Starred[i])
	}
	return subset
}

func generateOutputIfNewData(wbgStats *WallabagStats, total, archived, unread, starred float64) bool {
	if isDataSetNew(wbgStats, total, archived, unread, starred) && isDataValid(total, archived, unread, starred) {
		if *d {
			log.Println("found new stats data set")
		}
		wbgStats.Times = append(wbgStats.Times, time.Now())
		wbgStats.Total = append(wbgStats.Total, total)
		wbgStats.Unread = append(wbgStats.Unread, unread)
		wbgStats.Starred = append(wbgStats.Starred, starred)

		if *ddd {
			log.Printf("main: wbgStats: %v\n", wbgStats)
		}

		if *d {
			log.Print("writing data json file")
		}
		err := writeNewJSON(wbgStats)
		if err != nil {
			log.Println(err)
		}
		if !*dataOnly {
			if *d {
				log.Print("generating output")
			}
			generateOutput(wbgStats)
		} else {
			if *d {
				log.Print("not generating charts due to data-only flag")
			}
		}
		return true
	}
	return false
}

func getMinMax(numArr []float64) (min, max float64) {
	min = 0
	max = 0
	if len(numArr) > 0 {
		min = numArr[0]
		max = numArr[0]
		if len(numArr) > 1 {
			for i := 1; i < len(numArr); i++ {
				if numArr[i] < min {
					min = numArr[i]
				}
				if numArr[i] > max {
					max = numArr[i]
				}
			}
		}
	}
	return min, max
}

func generateChart(wbgStatsLastX WallabagStats, chartPath string) (isGenerated bool, TotalMin, TotalMax, TotalCurrent, UnreadMin, UnreadMax, UnreadCurrent float64, NumOfDataPoints int) {
	if *dd {
		log.Printf("generateChart: data sets in wbgStatsLastX=%v and file name to generate is %v", len(wbgStatsLastX.Times), chartPath)
	}

	// generate only if at least two data rows are available
	if len(wbgStatsLastX.Times) > 1 {
		err := generateChartPNG(&wbgStatsLastX, chartPath)
		if err != nil {
			isGenerated = false
		} else {
			isGenerated = true
		}
	}
	if *dd {
		log.Printf("generateChart: chartPath=%v, isGenerated=%v", chartPath, isGenerated)
	}
	TotalMin, TotalMax = getMinMax(wbgStatsLastX.Total)
	UnreadMin, UnreadMax = getMinMax(wbgStatsLastX.Unread)
	return isGenerated, TotalMin, TotalMax, wbgStatsLastX.Total[len(wbgStatsLastX.Times)-1], UnreadMin, UnreadMax, wbgStatsLastX.Unread[len(wbgStatsLastX.Times)-1], len(wbgStatsLastX.Times)
}

func generateOutput(wbgStats *WallabagStats) {
	err := CopyFSDir(templateFiles, directoryStatic, directoryOutput)
	if err != nil {
		log.Println("error while copying contents from", directoryStatic, "dir to", directoryOutput, "dir. Error:", err)
	}
	var cssIDsAll = [numberOfCharts]string{"twentyfourhours", "sevendays", "fourweeks", "twelvemonths", "overall"}
	var dataTitleAll = [numberOfCharts]string{"Last Day", "Last Week", "Last Month", "Last Year", "Overall"}
	var durationAll = [numberOfCharts]time.Duration{-24 * time.Hour, -7 * 24 * time.Hour, -30 * 24 * time.Hour, -365 * 24 * time.Hour, time.Since(wbgStats.Times[0])}
	var allStatsData [numberOfCharts]statsData
	for i := 0; i < numberOfCharts; i++ {
		// standard charts with all available data points
		allStatsData[i].CSSId = cssIDsAll[i]
		allStatsData[i].DataTitle = dataTitleAll[i]
		allStatsData[i].ChartPath = chartPaths[i]
		allStatsData[i].Dur = durationAll[i]
		allStatsData[i].IsGenerated,
			allStatsData[i].TotalMin,
			allStatsData[i].TotalMax,
			allStatsData[i].TotalCurrent,
			allStatsData[i].UnreadMin,
			allStatsData[i].UnreadMax,
			allStatsData[i].UnreadCurrent,
			allStatsData[i].NumOfDataPoints = generateChart(getWallabagStatsSubset(wbgStats, allStatsData[i].Dur), directoryOutput+string(os.PathSeparator)+allStatsData[i].ChartPath)
		allStatsData[i].GenTime = getLastModTime(directoryOutput + string(os.PathSeparator) + allStatsData[i].ChartPath)
		generateHTML(wbgStats, allStatsData)
	}
}
