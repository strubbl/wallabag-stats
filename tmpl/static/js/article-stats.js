var chartLabels = [];
var chartData = [];
var current_tab = null;
var allData = null;

function appendData (data, whichDiagram) {
  chart.data.labels = []
  chart.data.datasets[0].data = []
  chart.data.datasets[1].data = []
  switch (whichDiagram) {
    case "#LastXDays":
      for (let i = 0; i < data.LastXDays.length; i++) {
        chart.data.labels.push(data.LastXDays[i].Period);
        chart.data.datasets[0].data.push(Number(data.LastXDays[i].Minutes));
        chart.data.datasets[1].data.push(Number(data.LastXDays[i].Number));
      }
      break;
    case "#LastXMonths":
      for (let i = 0; i < data.LastXMonths.length; i++) {
        chart.data.labels.push(data.LastXMonths[i].Period);
        chart.data.datasets[0].data.push(Number(data.LastXMonths[i].Minutes));
        chart.data.datasets[1].data.push(Number(data.LastXMonths[i].Number));
      }
      break;
    case "#LastXYears":
      for (let i = 0; i < data.LastXYears.length; i++) {
        chart.data.labels.push(data.LastXYears[i].Period);
        chart.data.datasets[0].data.push(Number(data.LastXYears[i].Minutes));
        chart.data.datasets[1].data.push(Number(data.LastXYears[i].Number));
      }
      break;
  }
  chart.update();
}

fetch('data-articles-stats-charts.json', { cache: "no-cache" })
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    allData = data
    if (current_tab == null) {
      appendData(allData, "#LastXDays")
    }
    else {
      appendData(allData, current_tab.hash)
    }
  })
  .catch(function (err) {
    console.log(err);
  }
  );

var ctx = document.getElementById('chartLastX').getContext('2d');
ctx.canvas.width = window.innerWidth;
var chart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: [],
    datasets: [{
      backgroundColor: 'rgb(79, 230, 175)',
      data: [],
      label: "Reading time in minutes",
      order: 2
    },
    {
      borderColor: 'rgb(93, 138, 168)',
      backgroundColor: 'rgba(0, 0, 0, 0)',
      data: [],
      label: "Number of articles read",
      type: 'line',
      cubicInterpolationMode: 'monotone',
      order: 1
    }
    ]
  },
  options: {
    responsive: false,
  }
});



var triggerTabList = [].slice.call(document.querySelectorAll('#dateRangeTabs button'))
triggerTabList.forEach(function (triggerEl) {
  var tabTrigger = new bootstrap.Tab(triggerEl)

  triggerEl.addEventListener('click', function (event) {
    event.preventDefault()
    tabTrigger.show()
  })

  triggerEl.addEventListener('shown.bs.tab', function (event) {
    appendData(allData, event.target.getAttribute("data-bs-target"))
  })

})
