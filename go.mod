module codeberg.org/strubbl/wallabag-stats

go 1.23.0

toolchain go1.24.0

require (
	github.com/Strubbl/wallabago/v9 v9.0.10
	github.com/wcharczuk/go-chart/v2 v2.1.2
)

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	golang.org/x/image v0.25.0 // indirect
	golang.org/x/text v0.23.0 // indirect
)
