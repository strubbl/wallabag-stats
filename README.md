# wallabag-stats

Draws a chart for unread and total articles in your [wallabag](https://github.com/wallabag/wallabag/) instance.

Chart is only created when there at least two data sets, in which unread and total are both different compared with their previous value. Otherwise, if the delta is zero, the go-chart lib does not print a chart.

## Example

![Example chart](example/example.png)

## Install

Copy `example/config.json` along with your wallabag-stats binary and change it according to your wallabag instance.

### Updates via Cron

You can install a cronjob to fetch the stats data from your wallabag and let the graph being printed on changes. E.g. a user cronjob line by editing your cronjob file via `cronjob -e`:

```
* * * * * cd /home/$USER/opt/wallabag-stats/ && ./wallabag-stats && cp -r output/ /var/www
```

## wallabag-stats -h

```
Usage of ./wallabag-stats:
  -article-stats-start-date string
    	date when the article stats shall start, it has to be provided as string with the RFC3339 format 2006-01-02T15:04:05Z07:00 (default "2020-12-10T21:00:00+01:00")
  -config string
    	file name of config JSON file (default "config.json")
  -d	debug mode
  -data string
    	file name of data JSON file (default "data.json")
  -data-only
    	collect data only, do not generate any charts
  -dd
    	more debug mode
  -ddd
    	even more debug mode
  -f	force regeneration of charts and html even if no new data is present
  -v	print version
  -validate
    	get through every data item and sort invalid data entries out
```


## Go Report Card

[![Go Report Card](https://goreportcard.com/badge/codeberg.org/strubbl/wallabag-stats)](https://goreportcard.com/report/codeberg.org/strubbl/wallabag-stats)


## How to release

```
git tag x.y.z
git push --tags
goreleaser r --rm-dist
```
