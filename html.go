package main

import (
	"html/template"
	"log"
	"os"
	"strconv"
	"time"
)

var funcMap = template.FuncMap{
	"minus": minus,
}

func minus(a, b float64) string {
	return strconv.FormatFloat(a-b, 'f', -1, 64)
}

func writeTemplateToHTML(wbgStats *WallabagStats, templateName string, allStatsData [numberOfCharts]statsData) {
	if *dd {
		log.Printf("writeTemplateToHTML templateName=%v", templateName)
	}
	f, err := os.Create(directoryOutput + string(os.PathSeparator) + templateName + fileSuffixHTML)
	if err != nil {
		log.Println("writeTemplateToHTML", err)
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Println("writeTemplateToHTML", err)
		}
	}()

	var td templateData
	td.TableData = make([]dataRow, len(wbgStats.Times))
	var d dataRow
	for i := 0; i < len(wbgStats.Times); i++ {
		d = dataRow{i + 1, wbgStats.Times[i], wbgStats.Total[i], wbgStats.Unread[i], wbgStats.Starred[i]}
		td.TableData[i] = d
	}

	td.ChartDataStats = allStatsData
	td.GenTime = time.Now()
	td.TemplateName = templateName
	td.Version = version
	htmlSource, err := template.New(templateName+fileSuffixTemplate).Funcs(funcMap).ParseFS(templateFiles, directoryTemplates+"/"+templateName+fileSuffixTemplate, directoryTemplates+"/"+templateNameHeader+fileSuffixTemplate, directoryTemplates+"/"+templateNameNavigation+fileSuffixTemplate, directoryTemplates+"/"+templateNameScripts+fileSuffixTemplate, directoryTemplates+"/"+templateNameFooter+fileSuffixTemplate, directoryTemplates+"/"+templateNameFooterNoJS+fileSuffixTemplate)
	if err != nil {
		log.Println("writeTemplateToHTML", err)
	}
	htmlSource.Execute(f, td)
}

func generateHTML(wbgStats *WallabagStats, allStatsData [numberOfCharts]statsData) {
	if *dd {
		log.Println("generateHTML start")
	}
	writeTemplateToHTML(wbgStats, templateNameIndex, allStatsData)
	writeTemplateToHTML(wbgStats, templateNameArticleStats, allStatsData)

	if *dd {
		log.Println("generateHTML end")
	}
}
