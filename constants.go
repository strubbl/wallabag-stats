package main

import (
	"embed"
	"os"
)

const (
	chartDays int = iota // Day == 0
	chartMonths
	chartYears
)
const defaultArticleStatsStartDate = "2020-12-10T21:00:00+01:00"
const defaultConfigJSON = "config.json"
const defaultDataJSON = "data.json"
const defaultDataArticleStatsJSON = "data-articles.json"
const defaultDataArticleStatsChartsJSON = "data-articles-stats-charts.json"
const fileSuffixHTML = ".html"
const fileSuffixPNG = ".png"
const fileSuffixTemplate = ".tmpl"
const directoryOutput = "output"
const directoryTemplates = "tmpl"
const directoryStatic = directoryTemplates + string(os.PathSeparator) + "static"
const lockFile = ".lock"
const numberOfCharts = 5
const perPageLimit = 1000
const templateNameArticleStats = "article-stats"
const templateNameFooter = "footer"
const templateNameFooterNoJS = "footer-nojs"
const templateNameHeader = "header"
const templateNameIndex = "index"
const templateNameNavigation = "nav"
const templateNameScripts = "scripts"
const xDays = 30
const xMonths = 12
const xYears = 10

// non-const variables
var chartPaths = []string{
	"chart-day" + fileSuffixPNG,
	"chart-week" + fileSuffixPNG,
	"chart-month" + fileSuffixPNG,
	"chart-year" + fileSuffixPNG,
	"chart-overall" + fileSuffixPNG}

var (
	version = "dev"
	commit  = "none"
	date    = "unknown"
	builtBy = "unknown"
)

//go:embed all:tmpl
var templateFiles embed.FS
