package main

import "time"

// WallabagStats is a data set representing the number of total, unread and starred articles in wallabg at a given time
// This data format has been chosen to easily reuse the data for the axis in github.com/wcharczuk/go-chart. Otherwise
// it would have made more sense to save one data set and create an array of data sets
type WallabagStats struct {
	Times   []time.Time
	Total   []float64
	Unread  []float64
	Starred []float64
}

// ArticleStats saves the the wallabag article ID, its estimated reading duration and the time the article was read
type ArticleStats struct {
	ArticleID            int
	ReadingTimeInMinutes int
	ReadTime             time.Time
}

// LastXData holds the data for the last x time period of reading, e.g. amount of articles and minutes read within the last x days/months/years
type LastXData struct {
	Period  string
	Minutes int
	Number  int
}

// ArticleStatsChartData holds all data for the chart.js representation, which is generated from the array of ArticleStats
type ArticleStatsChartData struct {
	LastXDays   []LastXData
	LastXMonths []LastXData
	LastXYears  []LastXData
}

// do not change the order of dataRow struct, this breaks creating one data row during table data generation
type dataRow struct {
	No      int
	Times   time.Time
	Total   float64
	Unread  float64
	Starred float64
}

type templateData struct {
	ChartDataStats [numberOfCharts]statsData
	GenTime        time.Time
	TableData      []dataRow
	TemplateName   string
	Version        string
}

type statsData struct {
	ChartPath       string
	CSSId           string
	DataTitle       string
	Dur             time.Duration
	GenTime         time.Time
	IsGenerated     bool
	NumOfDataPoints int
	TotalMin        float64
	TotalMax        float64
	TotalCurrent    float64
	UnreadMin       float64
	UnreadMax       float64
	UnreadCurrent   float64
}
