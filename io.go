package main

import (
	"embed"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
)

func CopyFSDir(f embed.FS, src string, dst string) error {
	if _, err := os.Stat(dst); os.IsNotExist(err) {
		if err := os.MkdirAll(dst, os.ModePerm); err != nil {
			return fmt.Errorf("CopyFSDir: error creating directory %s: %v", dst, err)
		}
	}
	fileList, err := f.ReadDir(src)
	if err != nil {
		return fmt.Errorf("CopyFSDir: error reading directory %s: %v", src, err)
	}
	for _, file := range fileList {
		srcFileName := filepath.Join(src, file.Name())
		dstFileName := filepath.Join(dst, file.Name())
		if file.IsDir() {
			if err := CopyFSDir(f, srcFileName, dstFileName); err != nil {
				return fmt.Errorf("CopyFSDir: error copying subdirectory %s: %v", file.Name(), err)
			}
		} else {
			err = CopyFSFile(f, srcFileName, dstFileName)
			if err != nil {
				return fmt.Errorf("CopyFSDir: error while copying file %s from src to dst: %v", file.Name(), err)
			}
		}
	}
	return nil
}

func CopyFSFile(f embed.FS, src, dst string) (err error) {
	srcFile, err := f.Open(src)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during open:", err)
		return nil
	}
	defer srcFile.Close()
	dstFile, err := os.Create(dst)
	if err != nil {
		if *dd {
			// file might already exist
			log.Println("CopyFSFile: src", src, "dst", dst, "error during create:", err)
		}
		return nil
	}
	defer func() {
		if e := dstFile.Close(); e != nil {
			err = e
		}
	}()
	_, err = io.Copy(dstFile, srcFile)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during copy:", err)
		return nil
	}
	err = dstFile.Sync()
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during sync:", err)
		return nil
	}
	err = os.Chmod(dst, 0644)
	if err != nil {
		log.Println("CopyFSFile: src", src, "dst", dst, "error during chmod:", err)
		return nil
	}
	return nil
}

func readCurrentJSON(i interface{}) error {
	if *dd {
		log.Println("readCurrentJSON")
	}
	var jsonFileName string
	if *dd {
		log.Println("readCurrentJSON: given type:")
		log.Printf("%T\n", i)
	}
	switch i.(type) {
	case *WallabagStats:
		if *dd {
			log.Println("found *WallabagStats type")
		}
		jsonFileName = *dataJSON
	case *[]ArticleStats:
		if *dd {
			log.Println("found []ArticleStats type")
		}
		jsonFileName = defaultDataArticleStatsJSON
	default:
		if *d {
			log.Println("unkown type for reading json")
		}
		return errors.New("unkown type for reading json")
	}

	if jsonFileName == "" {
		return errors.New("no JSON file name given")
	}
	if _, err := os.Stat(jsonFileName); os.IsNotExist(err) {
		// in case file does not exist, we cannot prefill the WallabagStats
		if *d { // not fatal, just start with a new one
			log.Printf("file does not exist %s", jsonFileName)
		}
		return nil
	}
	b, err := os.ReadFile(jsonFileName)
	if err != nil {
		if *dd {
			log.Println("readCurrentJSON: error while ioutil.ReadFile", err)
		}
		return err
	}
	if len(b) <= 0 {
		return errors.New("jsonFileName " + jsonFileName + " is an empty file")
	}
	err = json.Unmarshal(b, i)
	if err != nil {
		if *d {
			log.Println("readCurrentJSON: error while json.Unmarshal", err)
		}
		return err
	}
	return nil
}

func writeNewJSON(i interface{}) error {
	if *dd {
		log.Println("writeNewJSON: given type:")
		log.Printf("%T\n", i)
	}
	var jsonFileName string
	switch i.(type) {
	case *WallabagStats:
		if *dd {
			log.Println("writeNewJSON: found *WallabagStats type")
		}
		jsonFileName = *dataJSON
	case []ArticleStats:
		if *dd {
			log.Println("writeNewJSON: found []ArticleStats type")
		}
		jsonFileName = defaultDataArticleStatsJSON
	case ArticleStatsChartData:
		if *dd {
			log.Println("writeNewJSON: found ArticleStatsChartData type")
		}
		if _, err := os.Stat(directoryOutput); os.IsNotExist(err) {
			os.Mkdir(directoryOutput, os.ModePerm)
		}
		jsonFileName = directoryOutput + string(os.PathSeparator) + defaultDataArticleStatsChartsJSON
	default:
		return errors.New("unkown data type for writing json")
	}

	b, err := json.Marshal(i)
	if err != nil {
		if *dd {
			log.Println("writeNewJSON: error while marshalling data json", err)
		}
		return err
	}
	if len(b) <= 0 {
		if *dd {
			log.Println("writeNewJSON: Marshalling the json leads to a size of zero for file", jsonFileName)
		}
		return errors.New("Marshalling the json leads to a size of zero for file " + jsonFileName)
	} else {
		if *dd {
			log.Println("writeNewJSON: writing json file", jsonFileName, "with a size of", len(b), "bytes")
		}
	}
	err = os.WriteFile(jsonFileName, b, 0644)
	if err != nil {
		if *dd {
			log.Println("writeNewJSON: error while writing data json", err)
		}
		return err
	}
	return nil
}
