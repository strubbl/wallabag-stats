package main

import (
	"log"
	"os"
	"time"
)

func printElapsedTime(start time.Time) {
	if *dd {
		log.Printf("printElapsedTime: time elapsed %.2fs\n", time.Since(start).Seconds())
	}
}

func removeLockFile(lf string) {
	if *dd {
		log.Printf("removeLockFile: trying to delete %s\n", lf)
	}
	err := os.Remove(lf)
	if err != nil {
		log.Printf("removeLockFile: error while removing lock file %s\n", lf)
		log.Panic(err)
	}
}

func saveValidatedData(curData *WallabagStats) error {
	var invalidIndexes []int
	if *dd {
		log.Println("wallabag stats data sets before:", len(curData.Times))
	}
	for i := 0; i < len(curData.Times); i++ {
		isValid := isDataValid((*curData).Total[i], (*curData).Total[i]-(*curData).Unread[i], (*curData).Unread[i], (*curData).Starred[i])
		if !isValid {
			invalidIndexes = append(invalidIndexes, i)
			log.Printf("found invalid data set: time=%v, total=%v, archived=%v, unread=%v, starred=%v\n", curData.Times[i], curData.Total[i], curData.Total[i]-curData.Unread[i], curData.Unread[i], curData.Starred[i])
		}
	}
	if len(invalidIndexes) > 0 {
		if *dd {
			log.Println("invalid indexes are:", invalidIndexes)
		}
		var newData WallabagStats
		lastIndex := -1
		for i := 0; i < len(invalidIndexes); i++ {
			// increase lastIndex by one to not include the invalid index anymore
			// also, for the first loop lastIndex shall be zero
			lastIndex++
			newData.Times = append(newData.Times, curData.Times[lastIndex:invalidIndexes[i]]...)
			newData.Total = append(newData.Total, curData.Total[lastIndex:invalidIndexes[i]]...)
			newData.Unread = append(newData.Unread, curData.Unread[lastIndex:invalidIndexes[i]]...)
			newData.Starred = append(newData.Starred, curData.Starred[lastIndex:invalidIndexes[i]]...)
			lastIndex = invalidIndexes[i]
			if lastIndex == invalidIndexes[len(invalidIndexes)-1] {
				// we reached the last invalid data set, so append the rest of the array now
				newData.Times = append(newData.Times, curData.Times[lastIndex+1:]...)
				newData.Total = append(newData.Total, curData.Total[lastIndex+1:]...)
				newData.Unread = append(newData.Unread, curData.Unread[lastIndex+1:]...)
				newData.Starred = append(newData.Starred, curData.Starred[lastIndex+1:]...)
			}
		}
		if *dd {
			log.Println("wallabag stats data sets after:", len(newData.Times))
		}
		err := writeNewJSON(&newData)
		return err
	}
	if *dd {
		log.Println("wallabag stats data sets after: same size of", len(curData.Times))
	}
	return nil
}
